var Map = function() {
    // Multiple Markers
    var map;
    var bounds;
    var markers;

    var initialize = function(){
        bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap',
            zoom: 13
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        map.setTilt(45);
        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        /*var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });*/
        $(document).trigger("maploaded");
    };

    var addMarkers = function(data){
        markers = data;
        // Info Window Content
        var infoWindowContent =
            '<div class="info_content">' +
            '<h3>{title}</h3>' +
            '<p>{content}</p>' + '</div>';

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow(), marker, i;

        // Loop through our array of markers & place each one on the map
        for( i = 0; i < markers.length; i++ ) {
            var position = new google.maps.LatLng(markers[i].location.lat, markers[i].location.lon);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i].title
            });

            // Allow each marker to have an info window
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infoWindow.setContent(infoWindowContent.replace("{title}",markers[i].title).replace("{content}",markers[i].content));
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            //map.fitBounds(bounds);
        }
    };

    var addYourPosition = function(lat, lon){
        var pinColor = "FF8200";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
        marker = new google.maps.Marker({
            position: {lat: lat, lng: lon},
            map: map,
            title: "You",
            icon: pinImage
        });
    };
    
    var setMapCenter = function(lat, lon){
        map.setCenter({lat: lat, lng: lon});
    };

    var getMap = function(){
        return map;
    };

    return {
        initialize: initialize,
        addMarkers: addMarkers,
        setMapCenter: setMapCenter,
        addYourPosition: addYourPosition
    }
};