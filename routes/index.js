var mongoose = require('mongoose');
var articleSchema = require('../model/article').articleSchema;
var Article = mongoose.model('Article', articleSchema);
var busboy = require('connect-busboy'); //middleware for form/file upload
var path = require('path');     //used for file path
var fs = require('fs-extra');       //File System - for file manipulation

var ugcSchema = require('../model/ugc').ugcSchema;
var Ugc = mongoose.model('Ugc', ugcSchema);

exports.index = function (req, res) {
    res.sendFile('/index.html');
};

exports.addUgc = function(req,res,next){
        var fstream;
        var ugcAuthor;
        var comment;
        var filename;
        console.log(ugcAuthor);
        console.log(comment);
        req.pipe(req.busboy);
        req.busboy.on('file', function (fieldname, file, name) {
            console.log("Uploading: " + name);
            filename = name;
            //Path where image will be uploaded
            fstream = fs.createWriteStream(__dirname + '/../public/ugc/' + name);
            file.pipe(fstream);
            fstream.on('close', function () {
                console.log("Upload Finished of " + name);
            });
        });
    req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
        console.log('Field [' + fieldname + ']: value: ' + val);
        if(fieldname== 'ugcauthor') {
            ugcAuthor = val;
        }
        if(fieldname == 'comment') {
            comment = val;
        }
    });
    req.busboy.on('finish', function() {
        console.log('Done parsing form!');
        console.log(ugcAuthor);
        console.log(comment);
        console.log(filename);
        var ugc = new Ugc({ugcAuthor:ugcAuthor, comment:comment, filename:filename});
        ugc.save(function (err, _article) {
            if (err) { next(err); }
            else{
                res.sendStatus(200);
            }
        });
    });
};

exports.getUgc = function(req,res,next){
    Ugc.find(function (err, ugcs) {
        if (err) { next(err); }
        else{
            res.json(ugcs);
        }
    });
};

exports.getArticles = function(req,res,next){
    Article.find(function (err, articles) {
    if (err) { next(err); }
    else{
        res.json(articles);
    }
    });
};

exports.addArticle = function(req,res,next){
    console.log(req.body);
    var article = new Article(req.body);
    article.save(function (err, _article) {
    if (err) { next(err); }
    else{
    res.sendStatus(200);
    }
    });
};

exports.clearArticles = function(req,res,next){
    Article.remove({},function(err){
    if (err) { next(err); }
    else{
    res.sendStatus(200);
    }
    });
};

exports.clearUgc = function(req,res,next){
    Ugc.remove({},function(err){
        if (err) { next(err); }
        else{
            res.sendStatus(200);
        }
    });
};