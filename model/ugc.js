var mongoose = require('mongoose');

var ugcSchema = mongoose.Schema({
    ugcAuthor: String,
    comment: String,
    filename: String
});

exports.ugcSchema = ugcSchema;