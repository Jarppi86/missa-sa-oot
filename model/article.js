var mongoose = require('mongoose');

var articleSchema = mongoose.Schema({
    time: Date,
    location: {
    	lat: Number,
    	lon: Number
    },
    title: String,
    content: String,
    author: String
});

exports.articleSchema = articleSchema;