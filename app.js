
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var request = require('request');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
mongoose.connect(process.env.CUSTOMCONNSTR_DB_URI || 'mongodb://localhost/misoot');
var articleSchema = require('./model/article').articleSchema;
var Article = mongoose.model('Article', articleSchema);
var busboy = require('connect-busboy'); //middleware for form/file upload

var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(busboy());

//just allow cors request because why would we block them
var allowCORS = function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Credentials",true);
    next();
};

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(allowCORS);
app.use(logger('dev'));
app.use(methodOverride());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

app.get('/', routes.index);

app.route('/api/articles/')
.get(routes.getArticles)
.post(routes.addArticle);

app.route('/api/ugc/')
    .get(routes.getUgc)
    .post(routes.addUgc);

app.get('/api/articles/clear/',routes.clearArticles);
app.get('/api/ugc/clear/',routes.clearUgc);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
